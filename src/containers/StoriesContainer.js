import React, {useEffect, useState, useCallback} from 'react';
import { Table } from "antd";
import { Bar } from "react-chartjs-2";
import { getStoryIds } from "../services/hackerNewsAPI";
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { GlobalStyle, StoriesContainerWrapper } from "../styles/StoriesContainerStyles";
import ClipLoader from "react-spinners/ClipLoader";
import 'antd/dist/antd.css'

const GET_DATA = gql`
{
getPredictionForCountry(id:183, numberOfDays:7){
	history{
			day
			count
		}
		predictions{
			dayFromFirstInfection
			prediction
		}
	}
}
`

const css = `position: relative;
left: 50%;
top: 50%;`

const columns = [
	{title: 'Country', dataIndex: 'country', key: 'country'},
	{title: 'Total Cases', dataIndex: 'totalC', key: 'totalC'},
	{title: 'New Cases', dataIndex: 'newC', key: 'newC'},
	{title: 'Total Deaths', dataIndex: 'totalD', key: 'totalD'},
	{title: 'New Deaths', dataIndex: 'newD', key: 'newD'},
	{title: 'Total Recovered', dataIndex: 'recovered', key: 'recovered'},
]

const data = [
	{
		key: 1,
		country: 'Serbia',
		totalC: '659',
		newC: '0',
		totalD: '11',
		newD: '1',
		recovered: '42',
		data: {
			labels: ['datum1', 'datum2', 'datum3'],
			datasets: [
				{
					label: "Nº effected",
					backgroundColor: '#b00b69',
					data: [50,150,500]
				}
			]
		}
	},
	{
		key: 2,
		country: 'USA',
		totalC: '123781',
		newC: '203',
		totalD: '2229',
		newD: '8',
		recovered: '3238',
		data: {
			labels: ['datum1', 'datum2', 'datum3'],
			datasets: [
				{
					label: "Nº effected",
					backgroundColor: '#b00b69',
					data: [50,150,500]
				}
			]
		}
	},
]

const expanddd = (expanded, record) => {
  console.log('expanded', expanded)
  console.log('record', record)
}

export const StoriesContainer = () => {
  const [loading, setLoading] = useState(true)
	// const { count } = useInfiniteScroll();
  // const [storyIds, setStoryIds] = useState([]);
  // const {loading,error,data} = useQuery(GET_DATA)


	useEffect(() => {
		console.log('e')
		// getStoryIds().then(data => setStoryIds(data));
  }, [])
  
  const onExpand = useCallback(
    (expanded, record) => {
      if (!expanded) {setLoading(true); return;}
      setTimeout(()=> {
        console.log('loading false')
        setLoading(false)
      },2000)
    }
  )

  // if (loading) return  

	return (
		<>
		<GlobalStyle/>
		<StoriesContainerWrapper data-test-id="stories-container">
		<h1>Haruspex</h1>
		<h2>COVID-19 Tracker and Predictor</h2>
		<Table
			pagination={false}
			columns={columns}
      dataSource={data}
      onExpand={onExpand}
			expandable={{
				expandedRowRender: i => {
          if (loading) return (<ClipLoader css={css} color={"#b00b69"} size={150} loading={loading} />)
          return (<Bar data={i.data} />)
        }
			}} 
		/>
		</StoriesContainerWrapper>
		</>
	);
} 