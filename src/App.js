import React from 'react';
import { ApolloProvider } from "@apollo/react-hooks";
import ApolloClient, { gql } from "apollo-boost";
import { StoriesContainer } from "./containers/StoriesContainer";
import { BASE_URL } from "./constants";

const client = new ApolloClient({
	uri: BASE_URL
})

// client.query({
// 	query: gql`
// 		{
//       getPredictionForCountry(id:183, numberOfDays:7){
//         history{
//           day
//           count
//         }
//         predictions{
//           dayFromFirstInfection
//           prediction
//         }
//       }
//     }
// 	`
// }).then(result =>console.log(result));

export const App = () =>(
	<ApolloProvider client={client}>
		<StoriesContainer/>
	</ApolloProvider>
);